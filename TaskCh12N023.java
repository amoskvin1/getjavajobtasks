package com.company;
import java.util.Scanner;

public class TaskCh12N023 {
private static int size;

    public static void main(String[] args) {
        //Заполнить двумерный массив размером 7x7 так, как показано на рис.12.1.
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("введите нечетный размер массива: ");
        size = sc.nextInt();
        }
        while (size % 2 == 0);
//********************************************************************************

        /** а) */
/*
        // создаем двумерный массив
        int[][] arr  = new int[size][size];

        // цикл по первой размерности
        for (int i = 0; i < size; i++) {
            // цикл по второй размерности

            for (int j = 0; j < size; j++) {

                if ((j <= size-i-1) && (j >= size-i-1) || (j<=i) && (j>=i))

                {
                    //инициализация элементов массива
                    arr[i][j] = 1;
                }
                //вывод элементов массива
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
*/
        /** б) */
/*
        // создаем двумерный массив
        int[][] arr  = new int[size][size];

        // цикл по первой размерности
        for (int i = 0; i < size; i++) {
            // цикл по второй размерности
            for (int j = 0; j < size; j++) {

                if ((j <= size-i-1) && (j >= size-i-1) || (j<=i) && (j>=i) || (j == size / 2) || (i == size / 2))
                {
                    //инициализация элементов массива
                    arr[i][j] = 1;
                }
                //вывод элементов массива
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
*/ 
        /** в) */

        // создаем двумерный массив
        int[][] arr  = new int[size][size];

        // цикл по первой размерности
        for (int i = 0; i < size; i++) {
            // цикл по второй размерности
            for (int j = 0; j < size; j++) {
                if ((j>=i) && (j <= size-i-1) ||
                        (j<=i) && (j >= size-i-1) ) {

                    //инициализация элементов массива
                    arr[i][j] = 1;
                }else{
                    arr[i][j] = 0;
                }
                //вывод элементов массива
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
    }
}