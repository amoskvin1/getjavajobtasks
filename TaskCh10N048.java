package com.company;
import java.util.Scanner;

public class TaskCh10N048 {
    private static int size;
    private static int start = 0;

    public static void main(String[] args) {
        // Написать  рекурсивную  функцию для  вычисления максимального элемента массива из n элементов

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        size = sc.nextInt();
        int arr[] = new int[size];

        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        System.out.println("Наибольший элемент массива = " + maxIndex(arr, start));
    }

    // функция находит наибольший елемент в массиве arr, начиная с первого
    private static int maxIndex(int[] arr, int start) {
        if (isMaxElement(arr, start))
            return start;
        return maxIndex(arr, start + 1);
    }

    // функция проверяет является ли число element наибольшим в массиве arr
    private static boolean isMaxElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > element)
                return false;
        }
        return true;
    }
}
