package com.company;
import java.util.Scanner;

public class TaskCh12N025 {
    private static int size, rows, cols;

    public static void main(String[] args) {
        // Заполнить двумерный массив так, как представлено на рис.12.3.
        Scanner sc = new Scanner(System.in);
        //System.out.print("введите размер массива: ");

        size = sc.nextInt();
        rows = size;
        cols = size;

//********************************************************************//

        /** а */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                arr[row][col] = x++;
            }
        }

        arrPrinting(arr);
*/

        /** б */
/*
        int[][] arr = new int[rows][cols];

        int x = 0;
        for (int col = 0; col < cols; ++col) {
            for (int row = 0; row < rows; ++row) {
                arr[row][col] = ++x;
            }
        }

    arrPrinting(arr);
*/

        /** в */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            for (int col = cols; col > 0; col--) {
                arr[row][col -1] = x++;
            }
        }

        arrPrinting(arr);
*/

        /** г */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            for (int col = cols; col > 0; col--) {
                arr[col -1][row] = x++;
            }
        }

    arrPrinting(arr);
*/

        /** д */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            if (row % 2 == 0) {
                for (int col = 0; col < cols; col++) {
                    arr[row][col] = x++;
                }
            } else {
                for (int col = cols; col > 0; col--) {
                    arr[row][col -1] = x++;
                }
            }
        }
        arrPrinting(arr);
 */

        /** е */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            if (row % 2 == 0) {
                for (int col = 0; col < cols; col++) {
                    arr[col][row] = x++;
                }
            } else {
                for (int col = cols; col > 0; col--) {
                    arr[col - 1][row] = x++;
                }
            }
        }

    arrPrinting(arr);
*/

        /** ж */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            for (int col = 0; col < cols; col++) {
                arr[row -1][col] = x++;
            }
        }

        arrPrinting(arr);
*/

        /** з */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            for (int col = 0; col < cols; col++) {
                arr[col][row -1] = x++;
            }
        }

    arrPrinting(arr);
*/

        /** и */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            for (int col = cols; col > 0; col--) {
                arr[row -1][col -1] = x++;
            }
        }

        arrPrinting(arr);
*/

        /** к */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            if (row % 2 == 0) {
                for (int col = cols; col > 0; col--) {
                    arr[row - 1][col - 1] = x++;
                }
            }else {
                for (int col = 0; col < cols; col++) {
                    arr[row - 1][col] = x++;
                }
            }
        }

        arrPrinting(arr);
*/

        /** л */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;

        for (int row = rows; row > 0; row--) {
            if (row % 2 != 0) {
                for (int col = cols; col > 0; col--) {
                    arr[row - 1][col - 1] = x++;
                }
            }else{
                for (int col = 0; col < cols; col++) {
                    arr[row - 1][col ] = x++;
                }
            }
        }

        arrPrinting(arr);
*/

        /** м */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = 0; row < rows; row++) {
            if (row % 2 != 0) {
                for (int col = 0; col < cols; col++) {
                    arr[row][col] = x++;
                }
            } else {
                for (int col = cols; col > 0; col--) {
                    arr[row][col -1] = x++;
                }
            }
        }
        arrPrinting(arr);
*/

        /** н */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            if (row % 2 == 0) {
                for (int col = 0; col < cols; col++) {
                    arr[col][row -1] = x++;
                }
            } else {
                for (int col = cols; col > 0; col--) {
                    arr[col - 1][row -1] = x++;
                }
            }
        }

    arrPrinting(arr);
*/

        /** о */
/*
        int[][] arr = new int[rows][cols];

        int x = 0;
        for (int col = 0; col < cols; ++col) {
            if (col % 2 == 0) {
                for (int row = rows; row > 0; --row) {
                    arr[row - 1][col] = ++x;
                }
            } else {
                for (int row = 0; row < rows; ++row) {
                    arr[row][col] = ++x;
                }
            }
        }
        arrPrinting(arr);
*/

        /** л */
/*
        int[][] arr = new int[rows][cols];

        int x = 1;

        for (int row = rows; row > 0; row--) {
            if (row % 2 == 0) {
                for (int col = cols; col > 0; col--) {
                    arr[row - 1][col - 1] = x++;
                }
            }else{
                for (int col = 0; col < cols; col++) {
                    arr[row - 1][col] = x++;
                }
            }
        }

        arrPrinting(arr);
*/

        /** р */

        int[][] arr = new int[rows][cols];

        int x = 1;
        for (int row = rows; row > 0; row--) {
            if (row % 2 != 0) {
                for (int col = 0; col < cols; col++) {
                    arr[col][row -1] = x++;
                }
            } else {
                for (int col = cols; col > 0; col--) {
                    arr[col - 1][row -1] = x++;
                }
            }
        }

        arrPrinting(arr);

    }

// вывод результата
        private static void arrPrinting(int[][] arr){
            for (int[] anArr : arr) {
                for (int anAnArr : anArr) {
                    System.out.print(anAnArr + " ");
                }
                System.out.println();
            }
        }
}
