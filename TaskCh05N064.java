package com.company;
import java.util.Scanner;

public class TaskCh05N064 {


    public static void main(String[] args) {

        //создаем массивы
        int peoples[] = new int[13];
        int area[] = new int[13];
        Scanner sc = new Scanner(System.in);

        //присваиваем значения элементам массива
        int i = 1, j = 1;

        for (; i < peoples.length; ) {
            System.out.print("Введите количество жителей " + i + "-ого района в тысячах: ");
            peoples[i] = sc.nextInt();
            i++;

            for (; j < area.length; ) {
                System.out.print("Введите площадь " + j + "-ого района: ");
                area[j] = sc.nextInt();
                j++;
                break;
            }
        }

        //Выводим значение средней плотности население
        System.out.println("средняя плотность населения по области в целом = " + population(peoples, area));
    }


    public static double population(int peoples[], int area[]) {

        //находим сумму людей всех районов
        double peoplesSum = 0;
        for (int sum : peoples){
            peoplesSum += sum;
        }

        //находим общую площадь районов
        double areaSum = 0;
        for (int sum : area){
            areaSum += sum;
        }

        //находим среднюю плотность
        double population = peoplesSum / areaSum;

        return population;
    }
}
