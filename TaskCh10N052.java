package com.company;

public class TaskCh10N052 {

    public static void main(String[] args) {
	// Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.
    int num = 789;

    revRec(num);
    }
    static void revRec(int num){
        if(num==0)
        {
            return;
        }
        System.out.print(num%10);
        revRec(num/10);
    }
}
