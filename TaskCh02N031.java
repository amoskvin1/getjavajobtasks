package com.company;
import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число от 100 до 999: ");
        int x = sc.nextInt();
        if(x > 100 && x < 999) {
            int a, n, c, res;
            a = x / 100;
            n = x % 10;
            c = ((x % 100) / 10);

            res = (a * 100) + (n * 10) + c;

            System.out.println(res);
        }else {
            System.out.println("Вы ввели число вне диапазона");
        }
    }
}
