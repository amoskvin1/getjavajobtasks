package com.company;

public class TaskCh10N050 {
    private static int n = 1, m = 3;

    public static void main(String[] args) {
	// Написать рекурсивную функцию для вычисления значения
    // так называемой функции Аккермана для неотрицательных чисел n и m.

        System.out.println(akkerman(n,m));
    }

    private static int akkerman(int n, int m){
        return n == 0 ? m + 1 : akkerman(n - 1, m == 0 ? 1 : akkerman(n, m - 1));
    }
}
