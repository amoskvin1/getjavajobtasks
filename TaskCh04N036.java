package com.company;

public class TaskCh04N036 {

    public static void main(String[] args) {
	// Работа светофора для пешеходов запрограммирована следующим образом:
    // в начале каждого часа в течение трех минут горит зеленый сигнал, затем в те-чение двух минут—красный,
    // в течение трех минут—опять зеленый и т.д.
    // Дано вещественное число t, означающее время в минутах, прошедшее с нача-ла очередного часа.
    // Определить, сигнал какого цвета горит для пешеходов в этот момент.

    /*
    //test 1
        int t = 3, res;
        res = t % 5;
        if(res < 3) {
            System.out.println("Green");
        }else if (res < 5){
            System.out.println("red");
        }
    */

   //test 2
        int t = 5, res;
        res = t % 5;
        if(res < 3) {
            System.out.println("Green");
        }else if (res < 5){
            System.out.println("red");
        }
    }
}
