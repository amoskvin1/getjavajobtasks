package com.company;
import java.util.Scanner;

public class TaskCh04N033 {

    public static void main(String[] args) {
	// Дано натуральное число.
    // а) Верно ли, что оно заканчивается четной цифрой?
    // б) Верно ли, что оно заканчивается нечетной цифрой?
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите целое число: ");
        int a;
        a = sc.nextInt();
        if (a % 2 == 0){
            System.out.println("Число заканчивается четной цифрой");
        }else{
            System.out.println("Число заканчивается нечетной цифрой");
        }
    }
}
