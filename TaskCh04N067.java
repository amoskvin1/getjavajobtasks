package com.company;

public class TaskCh04N067 {

    public static void main(String[] args) {
        //Дано целое число k Определить, каким будет k-й день года:
        // вы-ходным (суббота и воскресенье) или рабочим,если 1января—понедельник.

        //test1 in:5, workday;
        int k = 5;
        if ((k % 7 >1) && (k <=5)){
            System.out.println("Workday");
        }else{
            System.out.println("Weekend");
        }

        //test2 in 7, weekend;
        int k2 = 7;
        if ((k2 % 7 >1) && (k <=5)){
            System.out.println("Workday");
        }else {
            System.out.println("Weekend");
        }
    }
}
