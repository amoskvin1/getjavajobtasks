package com.company;
import java.lang.annotation.Documented;
import java.util.Scanner;

public class TaskCh03N029 {
    Scanner sc = new Scanner(System.in);


//СПЕРВА Я ПОДУМАЛ ЧТО ИСПОЛЬЗОВАНИЕ && || ТОЖЕ НЕЛЬЗЯ, ПОЭТОМУ РЕШИЛ С КОНСТРУКЦИЕЙ if-else. 
//ПОТОМ, КОГДА РАЗОБРАЛСЯ ТО УЖЕ ПОДХОДИЛ К КОНЦУ, НАДЕЮСЬ НЕ БУДЕТ НЕПРАВИЛЬНЫМ РЕШЕНИЕМ

    public static void main(String[] args) {
        /**
         * Записать условие, которое является истинным когда:
         */
        
        /**
         * Каждое из чисел xy нечетное
         */

        Scanner sc = new Scanner(System.in);
        int x, y;
        boolean b = false;
        System.out.println("Введите число x: ");
        x = sc.nextInt();
        if(x % 2 == 0){
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if(y % 2 == 0){
                b = true;
                System.out.println(b);
            }
        }


        /**
         * Только 1 из чисел xy <=0
         */

        int x, y;
        boolean b = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число x: ");
        x = sc.nextInt();
        if(x <= 2){
            System.out.println("Введите число y: ");
            y = sc.nextInt();
               if (y <=2){
                   System.out.println(b);
               }else{
                   b = true;
                   System.out.println(b);
               }
        }else {
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y <= 2) {
                b = true;
                System.out.println(b);
            }else{
                System.out.println(b);
            }
        }


    /**
     * Хотя бы 1 из чисел xy == 0
     */

        int x, y;
        boolean b = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число x: ");
        x = sc.nextInt();
        if(x == 0){
            b = true;
            System.out.println(b);
        }else {
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y == 0) {
                b = true;
                System.out.println(b);
            }else{
                System.out.println(b);
            }
        }


    /**
     *  Каждое из чисел xyz <=3
     */

        int x, y, z;
        boolean b = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число x: ");
        x = sc.nextInt();
        if(x <= 3){
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y <=3){
                System.out.println("Введите число z: ");
                z = sc.nextInt();
                if(z <= 3){
                    b = true;
                    System.out.println(b);
                }else{
                    System.out.println(b);
                }
            }else{
                System.out.println(b);
            }
        }else {
            System.out.println(b);

        }


    /**
     * Только 1 из чисел xyz кратно 5
     */

        int x, y, z;
        boolean b = false;
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число x: ");
        x = sc.nextInt();

        if(x % 5 == 0) {
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y % 5 == 0) {
                System.out.println(b);
            } else {
                System.out.println("Введите число z: ");
                z = sc.nextInt();
                if (z % 5 == 0) {
                    System.out.println(b);
                } else {
                    b = true;
                    System.out.println(b);
                }
            }
        }else{
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y % 5 == 0) {
                System.out.println("Введите число z: ");
                z = sc.nextInt();
                if (z % 5 == 0) {
                    System.out.println(b);
                } else {
                    b = true;
                    System.out.println(b);
                }
            }else{
                System.out.println("Введите число z: ");
                z = sc.nextInt();
                if (z % 5 == 0) {
                    b = true;
                    System.out.println(b);
                }else{
                    System.out.println(b);
                }
            }
        }


    /**
    * Хотя бы 1 из чисел xyz > 100
    */
        int x, y, z;
        boolean b = false;
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число x: ");
        x = sc.nextInt();

        if(x > 100){
            b = true;
            System.out.println(b);
        }else {
            System.out.println("Введите число y: ");
            y = sc.nextInt();
            if (y > 100) {
                b = true;
                System.out.println(b);
            }else {
                System.out.println("Введите число z: ");
                z = sc.nextInt();
                if (z > 100) {
                    b = true;
                    System.out.println(b);
                }else {
                    System.out.println(b);
                }
            }
        }
        
    }
}
