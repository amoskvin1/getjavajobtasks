package com.company;
import java.util.Scanner;

public class TaskCh09N107 {

    public static void main(String[] args) {
	// Дано слово. Поменять местами первую из букв аи последнюю из букво.
        // Учесть возможность того, что таких букв в слове может не быть.
       Scanner sc = new Scanner(System.in);

        System.out.print("Введите слово: ");
        String s = sc.next();

        StringBuilder strBldr = new StringBuilder(s);
        int a = strBldr.indexOf("а");
        int o = strBldr.lastIndexOf("о");

        if (a != -1 && o != -1) {
            strBldr.setCharAt(a, 'о');
            strBldr.setCharAt(o, 'а');
        }

        System.out.println(strBldr.toString());
    }
}
