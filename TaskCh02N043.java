package com.company;
import java.util.Scanner;


public class TaskCh02N043 {

    public static void main(String[] args) {
	//Даны два числа a и b. Если а делится на b и b делится на a то вывести 1,
    // иначе любое другое число. Неиспользовать условные операторы и операторы цикла.
     Scanner sc = new Scanner(System.in);
     int a, b, n;

        System.out.println("Введите число a: ");
        a = sc.nextInt();
        System.out.println("Введите число b: ");
        b = sc.nextInt();

        n = ((a)%(b))*((b)%(a))+1;

        System.out.println(n);
    }
}
