package com.company;
import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String[] args) {
	// Составить программу, которая печатает заданное слово, начиная с последней буквы.
        Scanner sc = new Scanner(System.in);
        String s;
        System.out.print("Введите слово: ");
        s = sc.next();

        //Выносим последний символ в отдельный char
        int last = s.length()-1;
        char ch = s.charAt(last);

        //Присваиваем последний символ слова
        StringBuilder strBldr = new StringBuilder(ch + s);
        strBldr.deleteCharAt(strBldr.length() -1);

        //Выводим результат
        System.out.println(strBldr.toString());
    }
}
