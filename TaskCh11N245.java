package com.company;
import java.util.Arrays;

public class TaskCh11N245 {

    private static int n = 0;
    public static void main(String[] args) {
        // Дан массив. Переписать его элементы в другой массив такого же размера следующим образом:
        // сначала должны идти все отрицательные элементы, а затем все остальные.
        // Использовать только один проход по исходному массиву.

        //создаем массивы
        int[] array = {1, 5, -6, 34, -48, -3, 10};
        int[] array2 = new int[array.length];
        int m = array.length-1;
        //вывод
        System.out.println(Arrays.toString(arrBuffer(array, array2,m)));
    }

        //проходим по массиву 1 присваивая значение массиву 2
    private static int[] arrBuffer(int[] array, int[] array2, int m) {
        for (int value : array) {
            if (value < 0) {
                array2[n] = value;
                n++;
            }
            else {
                array2[m] = value;
                m--;
            }
        }
    return array2;
    }
}