package com.company;
import java.util.Scanner;

public class TaskCh06N008 {
    //Дано число n. Из чисел 1, 4, 9, 16, 25,... напечатать те, которые не превыша-ют n

    public static void main(String[] args) {

        //получаем ввод пользователя
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n = sc.nextInt();

        //выполняем поиск максимального числа не превышающее число n
        for (int i = 1; i < n;) {
            if (i * i <= n) {
                System.out.println(i * i);
                i++;
            }else {
                break;
            }
        }
    }
}
