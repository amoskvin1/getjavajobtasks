package com.company;
import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args){
    Scanner sc = new Scanner(System.in);

    System.out.println("Введите число от 100 до 200: ");
    int numb = sc.nextInt();
    if(numb > 100 && numb < 200) {
        int a, b, c;
        a = numb / 100;
        b = numb % 10;
        c = ((numb % 100) / 10);

        numb = (b * 100) + (c * 10) + a;

        System.out.println(numb);
    }else {
        System.out.println("Вы ввели число вне диапазона");
            }
    }

}
