package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskCh10N055 {

    public static void main(String[] args) throws Exception {
        //Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную.
        //Значение N в основной программе вводится с клавиатуры (2 <= N <= 16)

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Введите число для конвертирования: ");
        String sNum = br.readLine();
        int i = Integer.parseInt(sNum);

        System.out.print("Выберите основание новой системы счисления: ");
        String sNumN = br.readLine();
        int N = Integer.parseInt(sNumN);

        System.out.println("В выбранной системе счисления " + i + " будет равно " + Integer.toString(i, N));
    }
}