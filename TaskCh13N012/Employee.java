package com.company;

public class Employee extends TaskCh13N012 {
    private String name;
    private String patronymic;
    private String surname;
    private String address;
    private int year;
    private int month;


    Employee(String name, String patronymic, String surname, String address, int year, int month) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.address = address;
        this.year = year;
        this.month = month;
    }


    public String toString() {
            if (todayYear - year >= 3) {
                return String.format("Имя: %s, Фамилия: %s, Отчество: %s\n Город: %s, Год: %d, Месяц: %d\n",
                        name, patronymic, surname, address, year, month);
            }

        return ("");
    }
}