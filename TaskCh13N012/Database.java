package com.company;
import java.util.ArrayList;

public class Database{
    private ArrayList <Employee> db;

    Database()
    {

        db = new ArrayList<>();
    }

    public void addEmployee(Employee empl)
    {
        db.add(empl);
    }

    public ArrayList<Employee> getDb()
    {
        return db;
    }
}
