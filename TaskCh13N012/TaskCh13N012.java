package com.company;
import java.util.ArrayList;

public class TaskCh13N012 {
    public static int todayYear = 2020, todayMonth = 03;


    public static void main(String[] args) {
        // Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество, адрес и дата поступления на работу (месяц, год).
        // Напечатать фамилию, имя, отчество и адрес сотрудников, которые на сегодняшний день прорабо-тали в фирме не менее трех лет.
        // День месяца не учитывать (при совпадении месяца поступления и месяца сегодняшнего дня считать, что прошел пол-ный год).

        Database db1 = new Database();

                db1.addEmployee(new Employee("Альберт", "Иванов", "Петрович", "Омск", 2019, 7));
                db1.addEmployee(new Employee("Сидор", "Сидоров", "Сидорович", "Урюпинск", 2000, 3));
                db1.addEmployee(new Employee("Антон", "Иванов", "Юсуфович", "Москва", 2012, 11));
                db1.addEmployee(new Employee("Михаил", "Иванов", "Петрович", "Задротск", 2019, 1));
                db1.addEmployee(new Employee("Роман", "Иванов", "Петрович", "Онтарио", 2018, 2));
                db1.addEmployee(new Employee("Дмитрий", "Иванов", "Петрович", "Торонто", 2017, 3));
                db1.addEmployee(new Employee("Хазан", "Иванов", "Петрович", "Минск", 2016, 4));
                db1.addEmployee(new Employee("Абдурахман", "Иванов", "Петрович", "Гродно", 2015, 5));
                db1.addEmployee(new Employee("Рита", "Иванов", "Петрович", "Сиэтл", 2014, 6));
                db1.addEmployee(new Employee("Николай", "Иванов", "Петрович", "Гамбург", 2013, 7));
                db1.addEmployee(new Employee("Кира", "Иванов", "Петрович", "Тверь", 2012, 8));
                db1.addEmployee(new Employee("Александр", "Иванов", "Петрович", "Нижний Новогород", 2011, 9));
                db1.addEmployee(new Employee("Виталий", "Иванов", "Петрович", "Калифорния", 2010, 10));
                db1.addEmployee(new Employee("Кембридж", "Иванов", "Петрович", "Сан-Франциско", 2009, 11));
                db1.addEmployee(new Employee("Ольг", "Иванов", "Петрович", "Смоленск", 2008, 12));
                db1.addEmployee(new Employee("Рус", "Иванов", "Петрович", "Куба", 2009, 11));
                db1.addEmployee(new Employee("Джамшут", "Иванов", "Петрович", "Новосибирск", 2010, 10));
                db1.addEmployee(new Employee("Русский", "Иванов", "Петрович", "Тула", 2012, 9));
                db1.addEmployee(new Employee("Нерусский", "Иванов", "Петрович", "Киев", 2014, 8));
                db1.addEmployee(new Employee("Ктото", "Иванов", "Петрович", "Санкт-Петербург", 2017, 3));



            ArrayList<Employee> myLocalDatabase = db1.getDb();

            for (Employee aMyDatabase : myLocalDatabase) {
                System.out.println(aMyDatabase.toString());
            }
    }
}