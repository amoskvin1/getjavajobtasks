package com.company;

import java.util.Scanner;

public class TaskCh12N024 {
    private static int size;

    public static void main(String[] args) {
        //Заполнить двумерный массив размером 7x7 так, как показано на рис.12.1.
        Scanner sc = new Scanner(System.in);
        System.out.println("введите размер массива: ");
        size = sc.nextInt();
//********************************************************************************//

        /** а) */
/*
        // создаем двумерный массив
        int[][] matrix = new int[size][size];

        for (int i = 0; i < size; i++)
        {
            matrix[i][0] = matrix[0][i] = 1;
        }

        // заполнение
        for (int i = 1; i < size; i++)
        {
            for (int j = i; j < size; j++)
            {
                matrix[i][j] = matrix[i][j - 1] + matrix[i - 1][j];
                if (i != j)
                {
                    matrix[j][i] = matrix[i][j];
                }
            }
        }

        // вывод результата
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
*/


        /** б) */

        // создаем двумерный массив
                int[][] a = new int[size][size];
                int x = 0;
        // заполнение
                for (int i = 0; i < a.length; i++) {
                    x++;
                    for (int j = 0; j < a[i].length; j++) {
                        a[i][j] = x++;
                        if (x > a[0].length) {
                        x = 1;
                        }
                    }
                }

        // вывод результата
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                System.out.print(" " + a[i][j]);
            }
            System.out.println();
        }
    }
}