package com.company;
import java.util.Scanner;

public class TaskCh09N166 {
    private static String strings;

    public static void main(String[] args) {

        // Дано предложение. Поменять местами его первое и последнее слово.
        System.out.println(solve(strings));
    }

    static String solve(String str) {
        Scanner sc = new Scanner(System.in);
        StringBuilder strBldr = new StringBuilder();
    //Просим ввод
        System.out.print("Введите предложение: ");
        str = sc.nextLine();

    //разбиваем ввод и присваиваем массиву
        String[] s = str.split(" ");

    //перебираем элементы и присваиваем середину стрингбилдеру
        for (int i = 1; i < s.length -1; i++) {
            strBldr.append(" " + s[i]);
        }

    //присваиваем стрингбилдеру последнее значение взятое из индека 0
    //и 0 значение взятое с конца (меняем слова местами)
        strBldr.append(" " + s[0]);
        strBldr.insert(0, s[s.length - 1]);
        return strBldr.toString();
    }
}
