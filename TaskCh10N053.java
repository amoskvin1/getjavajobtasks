package com.company;
import java.util.Scanner;
import java.util.Arrays;

public class TaskCh10N053 {
private static int[]array;
private static int size;

    public static void main(String[] args) {
	// Написать рекурсивную процедуру для ввода с клавиатуры последовательности чисел
    // и вывода ее на экран в обратном порядке (окончание последовательности — при вводе нуля).
        Scanner sc = new Scanner(System.in);

        System.out.print("введите размер массива: ");
        size = sc.nextInt();
        array = new int[size];

        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < size; i ++)
        {
            array[i] = sc.nextInt();
            if (array[i]== 0){
            return;
            }
        }
        System.out.println("перевернутый массив: " + Arrays.toString(reverseArray(array)));
    }

    static int[] reverseArray(int[] arr) {
        return reverseArray(arr, 0, new int[arr.length]);
    }

    static int[] reverseArray(int[] arr, int pos, int[] ret) {
        if ( pos < arr.length ) {
            ret[arr.length - pos - 1] = arr[pos];
            reverseArray(arr, pos + 1, ret);
        }
        return ret;
    }
}
