package com.company;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskCh10N056 {
private static int num;

    public static void main(String[] args) throws Exception {
        // Написать рекурсивную функцию, определяющую, является ли заданное натуральное  число  простым

        System.out.print("Введите число для проверки: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String sNum = br.readLine();
        num = Integer.parseInt(sNum);

        if (num > 2)
            System.out.println(primeCheck(num, 2));

    }

    // Метод проверки
    static boolean primeCheck(int num, int divider) {
        if (num == divider)
            return true;

        if (num % divider == 0)
            return false;

        else
            return primeCheck(num,divider +1);

    }
}