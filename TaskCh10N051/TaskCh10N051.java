package com.company;

public class TaskCh10N051 {
private static int n = 5;
    public static void main(String[] args) {
	// Определить  результат  выполнения  следующих  рекурсивных  процедур  при n = 5:

        //recursion1(n);
        //recursion2(n);
        recursion3(n);
    }

    //а)
    static void recursion1(int n){
        if(n > 0)
        {
            System.out.println(n);
        }
        recursion1(n-1);
    }

    //б)
    static void recursion2(int n){
        if(n > 0){
            recursion2(n-1);
        }
        System.out.println(n);
    }

    //в)
    static void recursion3(int n) {
        if (n > 0) {
            System.out.println(n);
        } else{
            recursion3(n - 1);
        }
    }

}
