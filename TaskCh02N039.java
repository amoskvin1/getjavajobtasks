package com.company;
import java.util.Scanner;

public class TaskCh02N039 {

    public static void main(String[] args) {
   //Даны целые числа (h, m, s) указывающие момент времени часов.
   // Определить угол в градусах между положением часовой стрелки в начале суток и в указанный момент времени

   //1 час = 30*
   //1 минута = 6*
   //1 секунда = 0,008* || 8 * 1000

        Scanner sc = new Scanner(System.in);
        int h = -1, m = - 1, s = - 1, angle = 0;

        while (h > 23 || h < 0) {
        System.out.println("Введите часы от 0 до 23: ");
        h = sc.nextInt();
        }
        while (m > 59 || m < 0) {
        System.out.println("Введите минуты от 0 до 59: ");
        m = sc.nextInt();
        }
        while (s > 59 || s < 0) {
        System.out.println("Введите секунды от 0 до 59: ");
        s = sc.nextInt();
        }

        angle = ((h * 30) + (m * 6) + (s * 8 / 1000));
        System.out.println("угол равен: " + angle);
    }
}
