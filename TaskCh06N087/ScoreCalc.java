package com.company;
import java.util.Scanner;

public class ScoreCalc {
   private static int value, team1Score, team2Score;


    public static void play() {
        Scanner sc = new Scanner(System.in);

    //выбор команды, завершение игры
        do {
            System.out.print("Enter team to score (1 or 2 or 0 to finish the game): ");
            value = sc.nextInt();
        } while ((value != 1) && (value != 2) && (value != 0));

    //ввод очков
        if (value == 1){
            do {
                System.out.print("введите количество очков от 1 до 3: ");
                value = sc.nextInt();
            }while(value < 0 || value > 3);

            team1Score += value;
            System.out.println(score());
            play();

        }else if(value == 2) {
            do {
                System.out.print("введите количество очков от 1 до 3: ");
                value = sc.nextInt();
            }while(value < 0 || value > 3);

            team2Score += value;
            System.out.println(score());
            play();

        }else {
            System.out.println(result());
        }
    }

    /**Intermediate score*/
    public static String score() {
        return "Счет: " + team1Score + " : " + team2Score;

    }

    /**result with winner, loser and their scores*/
     public static String result(){
         return "GameOver! The result is: " + Game.team1Name + " " + team1Score + " - " + Game.team2Name + " " + team2Score;
    }
}