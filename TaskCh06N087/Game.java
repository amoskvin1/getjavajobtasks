package com.company;
import java.util.Scanner;

public class Game {
    static String team1Name, team2Name;


    public static void main(String[] args) {
	/*
	  Составить программу, которая ведет учет очков, набранных каждой командой при игре в баскетбол.
      Количество очков, полученных командами в ходе игры, может быть равно 1, 2 или 3.
      После любого изменения счет выводить на экран.
      После окончания игры выдать итоговое сообщение и указать номер команды-победительницы.
      Окончание  игры  условно  моделировать  вводом количества очков, равного нулю.
	 */

     //принимаем название команд
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter team #1: ");
        team1Name = sc.next();
        System.out.println("Enter team #2: ");
        team2Name = sc.next();

        ScoreCalc.play();
    }
}
