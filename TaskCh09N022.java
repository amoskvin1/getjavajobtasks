package com.company;
import java.util.Scanner;

public class TaskCh09N022 {

    public static void main(String[] args) {
	// Дано слово, состоящее из четного числа букв. Вывести на экран его первую половину, не используя оператор цикла.
        Scanner sc = new Scanner(System.in);
        String s;

        System.out.print("Введите число с ЧЕТНЫМ количеством букв: ");
        s = sc.next();
        String s2 = s.substring(0,s.length()/2);

        if (s.length() %2 == 0){
            System.out.println(s2);
        }else{
            System.out.println("введенное слово состоит из НЕ ЧЕТНОГО количества букв");
        }

    }
}
