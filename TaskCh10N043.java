package com.company;
import java.util.Random;
public class TaskCh10N043 {

    public static void main(String[] args) {
    //Написать рекурсивную функцию:

        Random rndm = new Random();

        for (int i = 0; i < 10; i++){
            int val = rndm.nextInt(1000);
            System.out.println("число: " + val + " сумма цифр числа = " + sumOfDigits(val));
            System.out.println("количество цифр числа: " + val + " = " + numOfDigits(val) + "\n");

        }
    }

    //а)вычисление суммы цифр натурального числа
    private static int sumOfDigits(int val){
        if (val < 10) {
            return val;
        }
        return val % 10 + sumOfDigits(val / 10);
    }

    // б)вычисления количества цифр натурального числа.
    private static int numOfDigits(int val){
        if(val == 0){
            return 0;
        }else{
            return 1 + numOfDigits(val /10);
        }
    }
}