package com.company;
import java.util.Random;

public class TaskCh12N063 {
private static int[][] classes;
private static int sum;
    public static void main(String[] args) {
	// В двумерном массиве хранится информация о количестве учеников в том или ином классе каждой параллели школы
    // с первой по одиннадцатую (в первой строке—информация о количестве учеников в первых классах, во второй о вторых и т.д.).
    // В каждой параллели имеются 4класса. Определить среднее количество учеников в классах каждой параллели.

    //создаем массив и присваиваем значения
        classes = new int[11][4];
        for(int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                classes[i][j] = generateRandomIntIntRange(1,100);
            }
        }

        for (int i = 0; i < 11; i++) {
           sum = 0;
            for (int j = 0; j < 4; j++){
                sum += classes[i][j];
                sum = sum / 4;
            }

            //вывод
            System.out.println("на параллели " + (i+1) + " классов в среднем " + sum + " учеников");
        }
    }
    //выбираем случайное количество учеников
    public static int generateRandomIntIntRange(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
