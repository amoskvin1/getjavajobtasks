package com.company;
import java.util.Scanner;

public class TaskCh10N045 {

    private static int n;
    private static double a,b;

    public static void main(String[] args) {
        // Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
        Scanner sc = new Scanner(System.in);

        System.out.println("введите 1 член прогрессии: ");
        a = sc.nextDouble();

        System.out.println("введите разность прогрессии: ");
        b = sc.nextDouble();

        System.out.println("введите номер члена прогрессии: ");
        n = sc.nextInt();


        System.out.println("n-ый член прогрессии = " + progressionMem(a,b,n));

        System.out.println("сумма n членов прогрессии = " + progressionSum(a,b,n));
    }

    //а)n-го члена прогрессии;
    private static double progressionMem(double a, double b, int n){
        if (n == 1){
            return a;
        }
        return progressionMem(a + b,b,--n) ;
    }
    //б)суммы nпервых членов прогрессии.
    private static double progressionSum(double a, double b, int n){
        if (n == 1){
            return a;
        }
        return progressionSum(a + b,b,--n) + progressionMem(a,b,n) ;
    }
}
