package com.company;
import java.util.Scanner;

public class TaskCh10N047 {
private static int k;

    public static void main(String[] args) {
	// Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.
    // Последовательность Фибоначчи 12,  , ...ff образуется по закону: 11;f21;f12iiifff(i3, 4,...).

        Scanner sc = new Scanner(System.in);
        System.out.println("введите член последовательности: ");
        k = sc.nextInt();

        System.out.println(k + "-й член последовательности фибоначи = " + fibRecursion(k));
    }
    //метод нахождения члена
    private static int fibRecursion(int k) {
        return k < 2 ? k : fibRecursion(k - 1) + fibRecursion(k - 2);
    }
}
