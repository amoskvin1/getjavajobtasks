package com.company;
import java.util.Scanner;

public class TaskCh10N046 {
private static int n;
private static double a, b;

    public static void main(String[] args) {
	// Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию:
        Scanner sc = new Scanner(System.in);

        System.out.println("введите 1-ый член прогрессии: ");
        a = sc.nextDouble();

        System.out.println("введите знаменатель: ");
        b = sc.nextDouble();

        System.out.println("введите номер члена прогрессии: ");
        n = sc.nextInt();


        System.out.println("n-ый член прогрессии = " + progressionMem(a,b,n));

        System.out.println("сумма n членов прогрессии = " + progressionSum(a,b,n));
    }

    //а)нахождения n-го члена прогрессии
        private static double progressionMem(double a, double b, int n){
            if (n > 1){
                return progressionMem(a ,b,n-1) * b ;
            }else{
                return a;
            }
        }

        //б)нахождения суммы n первых членов прогрессии.
        private static double progressionSum(double a, double b, int n){
            if (n > 1){
                return progressionSum(a ,b,n-1) * b + progressionMem(a,b,n) ;
            }else{
                return a;
            }
        }
}

