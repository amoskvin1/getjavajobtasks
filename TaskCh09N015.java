package com.company;
import java.util.Scanner;

public class TaskCh09N015 {
    private static String s;
    private static int k;

    public static void main(String[] args) {
        // Дано слово. Вывести на экран его k-й символ.
        Scanner sc = new Scanner(System.in);

        System.out.print("Введите слово: ");
        s = sc.next();
        System.out.print("Введите # символа: ");
        k = sc.nextInt();

        if (k > s.length()) {
            System.out.println("вы превысили символы слова");
        } else {
            System.out.println(s.charAt(k-1));
        }
    }
}
