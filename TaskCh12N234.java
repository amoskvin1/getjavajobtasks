package com.company;

import java.util.Scanner;

public class TaskCh12N234 {
    private static int[][] arr;
    private static int size, rows, cols, Del, x;

    public static void main(String[] args) {
        // Дан двумерный массив.
        // а)Удалить из него k-ю строку.
        // б)Удалить из него s-й столбец.

        Scanner sc = new Scanner(System.in);
        System.out.print("размер массива?: ");

        size = sc.nextInt();
        rows = size;
        cols = size;
        arr = new int[rows][cols];
        x = 1;


        /**а*/

        do {
            System.out.print("введите номер строки для удаления: ");
            Del = sc.nextInt() - 1;
        }
        while (Del > arr.length - 2 || Del < 1);

        // заполнение
        x = 1;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (col == Del) {
                    arr[col + 1][row] = x++;
                }
                arr[col][row] = x++;

                if (col == arr[col].length - 1) {
                    arr[col][row] = 0;
                }
            }
        }
        arrOut(arr);


        /**б*/

        do {
            System.out.print("введите номер столбца для удаления: ");
            Del = sc.nextInt() - 1;
        }
        while (Del > arr.length - 1 || Del < 1);

        // заполнение
        x = 1;
        for (int col = 0; col < rows; col++) {
            for (int row = 0; row < cols; row++) {
                if (row == Del) {
                    arr[col][row + 1] = x++;
                }
                arr[col][row] = x++;

                if (row == arr.length - 1) {
                    arr[col][row] = 0;
                }
            }
        }
        arrOut(arr);
        System.out.println();
    }


    // вывод результата
    private static void arrOut(int[][] arr) {
        for (int[] anArr : arr) {
            for (int anAnArr : anArr) {
                System.out.print(anAnArr + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
