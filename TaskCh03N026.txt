//Вычислить значение логического выражения при всех возможных значениях логических величин X, Y, Z

xyz	subExpr1	subExpr2	res

000	!(X | Y)    &	(!X | !Z)	true;
001	!(X | Y)    &	(!X | !Z)	true;
010	!(X | Y)    &	(!X | !Z)	false;
011	!(X | Y)    &	(!X | !Z)	false;
100	!(X | Y)    &	(!X | !Z)	false;
101	!(X | Y)    &	(!X | !Z)	false;
110	!(X | Y)    &	(!X | !Z)	false;
111	!(X | Y)    &	(!X | !Z)	false;



000	!(!X & Y)   | 	(X & !Z)	true;
001	!(!X & Y)   | 	(X & !Z)	true;
010	!(!X & Y)   | 	(X & !Z)	false;
011	!(!X & Y)   | 	(X & !Z)	false;
100	!(!X & Y)   | 	(X & !Z)	true;
101	!(!X & Y)   | 	(X & !Z)	true;
110	!(!X & Y)   | 	(X & !Z)	true;
111	!(!X & Y)   | 	(X & !Z)	true;



000	X | !Y	&  !(X | !Z)	false;
001	X | !Y	&  !(X | !Z)	true;
010	X | !Y	&  !(X | !Z)	false;
011	X | !Y  &  !(X | !Z)	false;
100	X | !Y  &  !(X | !Z)	true;
101	X | !Y  &  !(X | !Z)	true;
110	X | !Y  &  !(X | !Z)	true;
111	X | !Y  &  !(X | !Z)	true;