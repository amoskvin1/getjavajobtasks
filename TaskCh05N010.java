package com.company;
import java.util.Scanner;

public class TaskCh05N010 {

    public static void main(String[] args) {

	//Напечатать таблицу перевода 1, 2,... 20 долларов США в рубли по текущему курсу
    // (значение курса вводится с клавиатуры)

        Scanner sc = new Scanner(System.in);
        int rate;

        System.out.print("Введите курс доллара: ");
        rate = sc.nextInt();

        for (int i = 1; i <= 20; i++){
            if (i > 1) {
                System.out.println("Курс " + i + " долларов = " + i * rate + " руб.");
            }else{
                System.out.println("Курс " + i + " доллара = " + i * rate + " руб.");
            }
        }



    }
}
