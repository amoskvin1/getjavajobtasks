package com.company;

public class TaskCh05N038 {

    public static void main(String[] args) {

        // а) На каком расстоянии от дома будет находиться мужчина после 100-го этапа
        // (если допустить, что такое возможно);

        // б) Какой общий путь он при этом пройдет.
        //Составляем цикл на момент которого человек уже прошел 2 этапа цикла, 0.5км от дома и 1.5 км в общем

        double fromHomeDist = 0.5; //расстояние от дома
        double toWorkDist = 1.0; //расстояние до работы
        double way = 1.5; //общая пройденная дистанция
        int n = 100; //количество этапов цикла

        /*
        Если я правильно понял, то он начинает метаться с все меньшим разбросом и все чаще передумывает
        пройдя отведенный отрезок пути.
        =>
        */

        for (int i = 1; i <= 100; i++) {
            if (i > 2) {
                if (i % 2 == 0) {
                    way += (toWorkDist - fromHomeDist) / 3;
                    fromHomeDist += (toWorkDist - fromHomeDist) / 3;

                } else if (i % 2 == 1) {
                    way += (toWorkDist - fromHomeDist) / 4;
                    fromHomeDist += (toWorkDist - fromHomeDist) / 4;

                }
            }
        }
        System.out.println("Общая пройденная дистанция: " + way + " Км.");
        System.out.println("Расстояние от дома: " + fromHomeDist + " Км.\n");

        //=> работу наш мужик любит больше
        //Расчеты делал исходя из той точки где он находится.
    }
}