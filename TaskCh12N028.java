package com.company;

import java.util.Scanner;

public class TaskCh12N028 {
    private static int size, rows, cols, dx, dy, dirChanges, visits;

    public static void main(String[] args) {
        // Заполнить  двумерный  массив  размером 55так,  как  представлено  на рис.12.4.

        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("введите размер массива: ");
            size = sc.nextInt();
        }
        while (size % 2 != 1);

        size *= size;
        rows = 0;
        cols = 0;
        dx = 1;
        dy = 0;
        dirChanges = 0;
        visits = size;

        int[][] matrix = new int[size][size];
//********************************************************************//

        for (int i = 0; i < size * size; i++) {
            matrix[rows][cols] = i + 1;
            if (--visits == 0) {
                visits = size * (dirChanges % 2) +
                        size * ((dirChanges + 1) % 2) -
                        (dirChanges / 2 - 1) - 2;
                int temp = dx;
                dx = -dy;
                dy = temp;
                dirChanges++;
            }
            cols += dx;
            rows += dy;
        }

        arrPrinting(matrix);

    }

    // вывод результата
    private static void arrPrinting(int[][] arr) {
        for (int[] anArr : arr) {
            for (int anAnArr : anArr) {
                System.out.print(anAnArr + " ");
            }
            System.out.println();
        }
    }
}