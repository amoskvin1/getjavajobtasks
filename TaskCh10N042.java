package com.company;
import java.util.Scanner;
public class TaskCh10N042 {
    private static double a;
    private static int n;

    public static void main(String[] args) {
        //  Написать рекурсивную функцию для расчета степени n вещественного числа a (n—натуральное число).
        Scanner sc = new Scanner(System.in);

        System.out.print("введите вещественное число a: ");
        a = sc.nextDouble();

        System.out.print("введите степень числа a: ");
        n = sc.nextInt();

        System.out.println("число а в степени n = " + fact(a, n));
    }

    public static double fact(double num, int power) {
        return ((power > 0) ? num * fact(num,power - 1) : 1);
    }
}
