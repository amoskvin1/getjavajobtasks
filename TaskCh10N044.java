package com.company;
import java.util.Scanner;

public class TaskCh10N044 {

    public static void main(String[] args) {
	// Написать рекурсивную функцию нахождения цифрового корня натурального числа.
        Scanner sc = new Scanner(System.in);
        System.out.println("введите натуральное число: ");
        int val = sc.nextInt();
        System.out.println("цифровой корень числа " + val + " = " + digiRoot(val));
    }

    static int digiRoot(int num){
        int sum = 0;
        int temp = num;
        while (temp > 0) {
            sum +=temp % 10;
            temp = temp / 10;
        }
        if (sum >= 10) {
            sum = digiRoot(sum);
        }
        return sum;
    }
}
