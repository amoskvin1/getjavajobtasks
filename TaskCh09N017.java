package com.company;
import java.util.Scanner;

public class TaskCh09N017 {

    public static void main(String[] args) {
	// Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же букву?
        String s;
        char a,z;
        Scanner sc = new Scanner(System.in);

        System.out.print("введите слово: ");
        s = sc.next();
        a = s.charAt(0);
        z = s.charAt(s.length() - 1);

        if (a == z){
            System.out.println("первый и последний символы РАВНЫ");
        }else{
            System.out.println("первый и последний символы НЕ РАВНЫ");
        }
    }
}
