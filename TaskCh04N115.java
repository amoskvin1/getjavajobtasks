package com.company;
import java.util.Scanner;

public class TaskCh04N115 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

    //1 ВАРИАНТ РЕШИЛ ЧЕРЕЗ МАССИВЫ-НАШЕЛ ПОДСКАЗКУ В ИНТЕРНЕТЕ, АДАПТИРОВАЛ ДЛЯ JAVA. 
    //2 ВАРИАНТ РЕШИЛ ЧЕРЕЗ ОПЕРАТОРЫ ВЫБОРА, ЧТО БЫ СООТВЕТСТВОВАТЬ ГЛАВЕ ЗАДАНИЯ.

    // а)значение n >= 1984

        int year;

        do {
            System.out.println("Введите год >= 1984: ");
            year = sc.nextInt();
        }while(year < 1984);

         String animals[] = {"обезьяна", "петух", "собака", "свинья", "крыса", "корова", "тигр", "заяц", "дракон", "змея", "лошадь", "овца"}; //12
         String elements[] = {"зеленый", "красный", "желтый", "белый", "черный"}; //10

         System.out.println(animals[(year - 4) % 12]);
         System.out.println(elements[((year - 4) % 10) / 2]);


    // б)значение n может быть любым натуральным числом

        int year2;

        System.out.println("Введите год: ");
        year2 = sc.nextInt();

        //Проверяем животное
        int num = ((year2 -4) % 12);
        switch (num){
            case (0):
                System.out.println("обезьяна");
                break;
            case (1):
                System.out.println("петух");
                break;
            case (2):
                System.out.println("собака");
                break;
            case (3):
                System.out.println("свинья");
                break;
            case (4):
                System.out.println("крыса");
                break;
            case (5):
                System.out.println("корова");
                break;
            case (6):
                System.out.println("тигр");
                break;
            case (7):
                System.out.println("заяц");
                break;
            case (8):
                System.out.println("дракон");
                break;
            case (9):
                System.out.println("змея");
                break;
            case (10):
                System.out.println("лошадь");
                break;
            case (11):
                System.out.println("овца");
                break;
        }

        //Проверяем цвет
        int num2 = ((year2 -4) % 10) / 2;
        switch (num2){
            case (0):
                System.out.println("зеленый");
                break;
            case (1):
                System.out.println("красный");
                break;
            case (2):
                System.out.println("желтый");
                break;
            case (3):
                System.out.println("белый");
                break;
            case (4):
                System.out.println("черный");
                break;
        }
    }
}
