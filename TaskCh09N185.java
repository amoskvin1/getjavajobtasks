package com.company;
import java.util.Scanner;
import java.util.Stack;

public class TaskCh09N185 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
	// а) вывести да или нет
    // б) В случае неправильности расстановки скобок:если имеются лишние правые (закрывающие) скобки,
    // то выдать сообщение с указанием позиции первой такой скобки;
    // если имеются лишние левые (открывающие) скобки, то выдать сообщение с указанием количества таких скобок.
    // Если скобки расставлены правильно, то сообщить об этом.

        System.out.print("введите арифметическое выражение: ");
        answer(sc.nextLine());
    }

    //метод вычислений
    static void answer(String source) {
        Stack<Character> brackets = new Stack();
        char[] chars = source.toCharArray();

        int flagger = -1;

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(') brackets.push(chars[i]);
            if (chars[i] == ')') {
                if ((brackets.empty() || brackets.peek() == ')') && flagger == -1) {
                    flagger = i;
                }
                if (!brackets.empty()) {
                    brackets.pop();
                }
            }
        }

        //вывод результата проверки
        if (brackets.empty() && flagger == -1){
            System.out.println("В строке НЕТ не правильно расставленных скобок: ");
        }if (!brackets.empty()){
            System.out.println("количество лишних скобок '(' = " + brackets.size());
        }if (flagger != -1){
            System.out.println("лишняя скобка ')' по индексу = " + flagger);
        }
    }
}
