package com.company;
import java.util.Scanner;
import java.util.Arrays;

public class TaskCh10N158 {

        public static void main(String[] args) {
        //Удалить из массива все повторяющиеся элементы, оставив их первые вхождения, 
        //т.е. в массиве должны остаться только различные элементы.
        
            Scanner sc = new Scanner(System.in);

            System.out.print("введите размер массива: ");
            int size = sc.nextInt();
            int[] array = new int[size];

            System.out.println("Введите элементы массива: ");
            for (int i = 0; i < size; i ++)
            {
                array[i] = sc.nextInt();
            }

        //вывод
            System.out.println(Arrays.toString(array));
            System.out.println(Arrays.toString(deduplicate(array)));
        }

        //проверка на повтор
        private static int[] deduplicate(int[] array) {
            int[] result = new int[array.length];
            int n = 0;
            for (int value : array) {
                if (notContains(result, n, value)) {
                    result[n++] = value;
                }
            }
            return deArrOut(result, n);
        }
        
        private static boolean notContains(int[] array, int bound, int i) {
            return !contains(array, bound, i);
        }
    
        private static boolean contains(int[] array, int bound, int i) {
            for (int j = 0; j < bound; j++) {
                if (array[j] == i) {
                    return true;
                }
            }
            return false;
        }
        //создаем новый массив и передаем ему длину 1 массива и результат проверки
        private static int[] deArrOut(int[] array, int size) {
            int[] deArr = new int[array.length];
            System.arraycopy(array,0, deArr, 0, size);
            return deArr;
        }
}
