package com.company;
import java.util.Scanner;
public class TaskCh10N041 {

    public static void main(String[] args) {
	//  Написать рекурсивную функцию для вычисления факториала натурального числа n.
        Scanner sc = new Scanner(System.in);
        System.out.print("введите натуральное число n: ");
        long n = sc.nextInt();
        System.out.println("факториал числа " + n + " равен: " + fact(n));

    }
    public static long fact(long num) {
        return ((num > 1) ? num * fact(num - 1) : 1);
    }
}
