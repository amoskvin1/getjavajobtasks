package com.company;

//Создание класса предка (принцип абстракции. Общая форма для реализуемых классов)//
abstract class Person {
    private String name;

    public String getName() {
        return name;
    }

    public Person(String name) {
        this.name = name;
    }

    interface greetings{
        void hello();
    }
}

//частный случай полиморфизма: 1 интерфейс - много форм
interface DescribeExperience {

    void describeExperience();
}


//Создание класса работодателя//

class Employer extends Person {

    public Employer(String name) {
        super(name);
    }

    public void hello() {
        System.out.printf(super.getName());
    }
}


//Создание класса кандидатов (принцип наследования)//
class Candidate extends Person  implements DescribeExperience {

    public Candidate(String name) {
        super(name);
    }

    public void hello() {
        System.out.printf(super.getName());
    }

    public void describeExperience() {

    }
}


//Создание класса GetJavaJob кандидатов//

class GetJavaJob extends Candidate {

    public GetJavaJob(String name) {
        super(name);
    }

    public void hello() {
        System.out.printf("Hi! my name is " + super.getName());
    }

    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews ");
    }
}


//Создание класса SelfLearners кандидатов//

class SelfLearners extends Candidate {

    public SelfLearners(String name) {
        super(name);
    }

    public void hello() {
        System.out.printf("Hi! my name is " + super.getName());
    }

    public void describeExperience() {
        System.out.println("I have been learning Java by myself, nobody examine how thorough is my knowledge and how good is my code. ");
    }
}



