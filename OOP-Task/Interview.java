package com.company;

import java.util.ArrayList;

public class Interview {

    public static void main(String[] args) {

        //создание экземпляра класса работодателя
        Employer employer = new Employer("Hi! introduce yourself and describe your experience please\n" );

        //создание экземпляра класса "базы данных" имен кандидатов
        CandidatesDb cDb = new CandidatesDb();

/*добавление в массив кандидатов*/
        cDb.addCandidate(new SelfLearners("selfLearn1\n"));
        cDb.addCandidate(new SelfLearners("selfLearn2\n"));
        cDb.addCandidate(new SelfLearners("selfLearn3\n"));
        cDb.addCandidate(new SelfLearners("selfLearn4\n"));
        cDb.addCandidate(new SelfLearners("selfLearn5\n"));
        cDb.addCandidate(new GetJavaJob("getJavaJob1\n"));
        cDb.addCandidate(new GetJavaJob("getJavaJob2\n"));
        cDb.addCandidate(new GetJavaJob("getJavaJob3\n"));
        cDb.addCandidate(new GetJavaJob("getJavaJob4\n"));
        cDb.addCandidate(new GetJavaJob("getJavaJob5\n"));

        //создаем новый массив добавленных имен
        ArrayList<Candidate> candidates = cDb.getCandidates();

        //проходим по листу имен и выводим их вместе с методами
        for (Candidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
            System.out.println("");
        }
    }
}
