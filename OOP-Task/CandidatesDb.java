package com.company;
import java.util.ArrayList;

//инкапсулируем арэй лист через геттер и сеттер что бы избежать нежелательного доступа к его методам
public class CandidatesDb {

    private ArrayList <Candidate> candidates = new ArrayList<>();

    public void addCandidate(Candidate name){
        candidates.add(name);
    }

    public ArrayList <Candidate> getCandidates(){
        return candidates;
    }
}
